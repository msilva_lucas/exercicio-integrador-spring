## Exercício Integrador Spring | PUC-RS - Sicredi

#### Estudante
  - matricula
  - nome
  - endereco
  - nmr_documento

#### Regras do sistema | Estudante

    - Ao cadastrar um estudante, o mesmo recebe um número de matrícula para evitar duplicações.
    - Consulta-se um estudante pelo número de matrícula.
    - Consulta-se um estudante por um pedaço do nome. Se houver mais de um "match", retorna uma lista.
    - Consulta-se a lista de todos os estudantes.

#### Disciplinas
  - cod_disciplina
  - nome_disciplina
  - horario_disciplina
  - cod_turma_disciplina

#### Regras do sistema | Disciplinas

    - Cadastrar uma disciplina, com os dado: cod_disciplina, nome_disciplina, horario_disciplina, cod_turma_disciplina.
